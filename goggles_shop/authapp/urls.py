from django.conf.urls import url
from django.urls import path

import authapp.views as authapp
from .views import UserDelete, UserDetail


app_name = 'authapp'

urlpatterns = [
url(r'^login/$', authapp.login, name='login'),
url(r'^logout/$', authapp.logout, name='logout'),
url(r'^register/$', authapp.register, name='register'),
url(r'^edit/$', authapp.edit, name='edit'),
path('<int:pk>/delete', UserDelete.as_view(), name='user_delete'),
path('<int:pk>/', UserDetail.as_view(), name='user_detail'),
]