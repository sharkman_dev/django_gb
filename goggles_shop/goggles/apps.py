from django.apps import AppConfig


class GogglesConfig(AppConfig):
    name = 'goggles'
