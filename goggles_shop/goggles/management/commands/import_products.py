from django.core.management.base import BaseCommand
import json
from os.path import basename

from goggles.models import ProductCategory
from goggles.models import Product



class Command(BaseCommand):
    def handle(self, *args, **options):
        with open('goggles/products.json') as f:
            products_dict = json.load(f)

        for key in products_dict.keys():
            new_category = ProductCategory(name=key)
            new_category.save()
            for value in products_dict[key]:
                new_product = Product(
                    category=new_category,
                    name=value,
                    price=float(products_dict[key][value]['price']),
                    image=basename(products_dict[key][value]['image']),
                )
                new_product.save()
