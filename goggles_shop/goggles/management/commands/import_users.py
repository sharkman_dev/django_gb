from django.core.management.base import BaseCommand

from authapp.models import ShopUser


class Command(BaseCommand):
    def handle(self, *args, **options):
        super_user = ShopUser.objects.create_superuser(
            'django',
            'django@geekshop.local',
            'geekbrains',
            age=33
        )
        super_user = ShopUser.objects.create_superuser(
            'test',
            'django@geekshop.local',
            'test',
            age=33
        )
