from django.shortcuts import render, redirect
from django.shortcuts import get_list_or_404

from django.views.generic import (
    CreateView, UpdateView,
    DeleteView, ListView, DetailView,
)

from django.views import View
from django.urls import reverse_lazy
from django.http import JsonResponse


from .models import ProductCategory, Product
from .forms import ProductModelForm, CategoryModelForm

# Create your views here.

def error404(request):
    return render(request, 'goggles/404.html', {})

def about(request):
    return render(request, 'goggles/about.html', {})

def checkout(request):
    return render(request, 'goggles/checkout.html', {})

def contact(request):
    return render(request, 'goggles/contact.html', {})

def customer(request):
    return render(request, 'goggles/customer.html', {})

def index(request):
    content = {
        'goggles_s': Product.objects.filter(category__name='goggles_s'),
        'goggles_m': Product.objects.filter(category__name='goggles_m'),
    }

    return render(request, 'goggles/index.html', content)

def goggles_s_list(request):
    query = get_list_or_404(Product)
    data = map(lambda itm: {
        'id': itm.id,
        'name': itm.name,
        'category': itm.category.name,
        'image': f'/media/{itm.image.name}',
        'price': itm.price,
    }, query)
    return JsonResponse({'results': list(data), 'count': len(query)})


def goggles_m_list(request):
    query = get_list_or_404(Product)
    data = map(lambda itm: {
        'id': itm.id,
        'name': itm.name,
        'category': itm.category.name,
        'image': f'/media/{itm.image.name}',
        'price': itm.price,
    }, query)
    return JsonResponse({'results': list(data), 'count': len(query)})



def payment(request):
    return render(request, 'goggles/payment.html', {})

def shop(request):
    return render(request, 'goggles/shop.html', {})

def single(request):
    return render(request, 'goggles/single.html', {})


class ProductDetail(DetailView):
    model = Product
    template_name = 'goggles/detail.html'
    context_object_name = 'instance'


class ProductCreate(CreateView):
    model = Product
    form_class = ProductModelForm
    template_name = 'goggles/create.html'
    success_url = reverse_lazy('index')


class ProductDelete(DeleteView):
    model = Product
    context_object_name = 'instance'
    template_name = 'goggles/delete.html'
    success_url = reverse_lazy('index')


class ProductUpdate(UpdateView):
    model = Product
    form_class = ProductModelForm
    template_name = 'goggles/create.html'
    success_url = reverse_lazy('index')



class CategoryDetail(DetailView):
    model = ProductCategory
    template_name = 'goggles/category_detail.html'
    context_object_name = 'instance'

class CategoryCreate(CreateView):
    model = ProductCategory
    form_class = CategoryModelForm
    template_name = 'goggles/create.html'
    success_url = reverse_lazy('index')

class CategoryDelete(DeleteView):
    model = ProductCategory
    context_object_name = 'instance'
    template_name = 'goggles/category_delete.html'
    success_url = reverse_lazy('index')

class CategoryUpdate(UpdateView):
    model = ProductCategory
    form_class = CategoryModelForm
    template_name = 'goggles/create.html'
    success_url = reverse_lazy('index')
