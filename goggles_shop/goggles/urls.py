from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

rest_endpoints = [
    path('api/products/goggles_s_list/', views.goggles_s_list, name='list_goggles_s'),
    path('api/products/goggles_m_list/', views.goggles_m_list, name='list_goggles_m'),
]

urlpatterns = [
    path('404/', views.error404, name='error404'),
    path('about/', views.about, name='about'),
    path('checkout/', views.checkout),
    path('contact/', views.contact, name='contact'),
    path('customer/', views.customer, name='customer'),
    path('', views.index, name='index'),
    path('payment/', views.payment),
    path('shop/', views.shop, name='shop'),
    path('single/', views.single, name='single'),
    path('<int:pk>/', views.ProductDetail.as_view(), name='detail'),

    path('category/<int:pk>/', views.CategoryDetail.as_view(), name='category_detail'),
    path('category_delete/<int:pk>/', views.CategoryDelete.as_view(), name='category_delete'),
    path('category_update/<int:pk>/', views.CategoryUpdate.as_view(), name='category_update'),
    path('category_create/', views.CategoryCreate.as_view(), name='category_create'),

    path('delete/<int:pk>/', views.ProductDelete.as_view(), name='delete'),
    path('update/<int:pk>/', views.ProductUpdate.as_view(), name='update'),
    path('create/', views.ProductCreate.as_view(), name='create'),
] + rest_endpoints


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
