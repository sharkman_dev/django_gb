# from django.db import models
from django.db.models import CharField, Model, TextField, ForeignKey, CASCADE, ImageField, DecimalField

# Create your models here.
class ProductCategory(Model):
    name = CharField(max_length = 50, unique=True)
    description = TextField(blank=True)

    def __str__(self):
        return self.name


class Product(Model):
    category = ForeignKey(ProductCategory, on_delete=CASCADE)
    name = CharField(max_length=50, unique=True)
    image = ImageField(blank=True)
    price = DecimalField(max_digits=8, decimal_places=2, default=0)

    def __str__(self):
        return f'{self.name} ({self.category.name})'

