from django import forms

from .models import Product, ProductCategory

# from images.models import Image


class CategoryForm(forms.Form):

    title = forms.CharField(
        label='title',
        widget=forms.widgets.TextInput(
            attrs={'class': 'form-control'}
        )
    )


class ProductModelForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'category', 'image', 'price']



class CategoryModelForm(forms.ModelForm):
    class Meta:
        model = ProductCategory
        fields = ['name', 'description',]